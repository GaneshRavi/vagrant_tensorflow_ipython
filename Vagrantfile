# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 8888, host: 18888

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    # vb.gui = true
    # Customize the amount of memory on the VM:
    vb.customize ["modifyvm", :id, "--memory", "2048"]
	vb.customize ["modifyvm", :id, "--cpus", 2]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update
	sudo apt-get install -y vim
	sudo su - root -c 'apt-get install -y python-pip python-dev build-essential'
	sudo su - root -c 'pip install --upgrade pip'
	sudo su - root -c 'pip install -U numpy'
	sudo pip install scipy
	sudo apt-get install -y python-matplotlib
	sudo pip install -U scikit-learn
	sudo pip install --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.8.0rc0-cp27-none-linux_x86_64.whl
	sudo pip install ipython
	sudo pip install jupyter

	sudo su - vagrant -c 'openssl req -x509 -nodes -days 365 -newkey rsa:1024 -subj "/C=UK/ST=./L=./O=./CN=." -keyout mykey.key -out mycert.pem'
	
	sudo mkdir /home/vagrant/.jupyter
	sudo chmod 700 /home/vagrant/.jupyter
	sudo cp /vagrant/jupyter_notebook_config.py /home/vagrant/.jupyter/jupyter_notebook_config.py
	sudo chown -R vagrant:vagrant /home/vagrant/.jupyter
	sudo chmod 664 /home/vagrant/.jupyter/jupyter_notebook_config.py
	#Run "jupyter notebook" and then access "https://192.168.0.234:8888"
   SHELL
   
   config.vm.define "tensor" do |nltk|
    nltk.vm.hostname = "tensor"
    nltk.vm.network "public_network", ip: "192.168.0.234"
  end
   
end
