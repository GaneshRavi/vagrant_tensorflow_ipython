Tensorflow VM with iPython notebook
=====================================
This is a Vagrantfile to create the Ubuntu environment to use for the Udacity Deep Learning course which requires Python 2.7, Tensorflow and iPython Notebook.


### Steps to create the environment

* Set the required public IP address for the VM, depending on the subnet you are in. Example: If you're on the subnet 192.168.0.0/24, then provide any valid 192.168.0.1 - 192.168.0.254 in the Vagrantfile.

* Set the required number of cores and RAM for the VM in the Vagrant file

* Build and start the VM with the command `vagrant up`

* SSH into the VM onto the provided IP address and the credentials vagrant/vagrant

* Start iPython notebook by executing the command `jupyter notebook`

* Open a browser, from your host machine and access the URL `https://192.168.0.234:8888`

* The username to access the webpage is set as `adminuser`